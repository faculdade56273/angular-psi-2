import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { Pet } from '../pet';
import { PetService } from '../pet.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css'],
})
export class HeroDetailComponent {
  hero!: Hero;

  pets: Pet[] = [];

  pet: Pet | undefined;

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location,
    private petService: PetService
  ) {}

  ngOnInit(): void {
    this.getHero();
    this.getPets();

    for (const pet of this.pets) {
      if (pet._id === this.hero.pet) {
        this.pet = pet;
      }
    }
  }

  getPets() {
    this.petService.getPets().subscribe((pets) => (this.pets = pets));
  }

  getHero(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.heroService.getHero(id).subscribe((hero) => (this.hero = hero));
    }
  }

  save(): void {
    if (this.hero) {
      this.heroService.updateHero(this.hero).subscribe(() => this.goBack());
    }
  }

  goBack(): void {
    this.location.back();
  }

  toggleDropdown(): void {
    var dropMenu = document.getElementById('dropdown-menu');

    if (dropMenu) {
      if (dropMenu.style.display != 'block') {
        console.log('Dropdown show');
        dropMenu.style.display = 'block';
      } else {
        console.log('Dropdown hide');
        dropMenu.style.display = 'none';
      }
    }
  }

  setPet(pet: Pet): void {
    this.hero.pet = pet._id;
    this.pet = pet;
  }

  removePet(): void {
    this.hero.pet = undefined;
  }
}
