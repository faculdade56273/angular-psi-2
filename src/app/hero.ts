import { Pet } from "./pet";

export interface Hero {
    _id: String;
    name: string;
    pet: String | undefined;
  }