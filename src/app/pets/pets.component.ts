import { Component } from '@angular/core';
import { MessageService } from '../message.service';
import { Pet } from '../pet';
import { PetService } from '../pet.service';

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.css'],
})
export class PetsComponent {

  constructor(
    private petService: PetService,
    private messageService: MessageService
  ) {}

  pets: Pet[] = [];

  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.petService.addPet({ name } as Pet).subscribe((pet) => {
      this.pets.push(pet);
      this.messageService.add(`PetService: Added? ${pet.name}`);
    });
  }

  delete(pet: Pet): void {
    this.pets = this.pets.filter((h) => h !== pet);
    this.petService.deletePet(pet._id).subscribe();
  }

  ngOnInit(): void {
    this.getPets();
  }

  getPets(): void {
    this.petService.getPets().subscribe((pets) => (this.pets = pets));
  }

  sortPets(): void {
    this.petService.getPets().subscribe((pets) => (this.pets = pets.sort(
      (a, b) => a.name.localeCompare(b.name)
    )));
  }
}
